#!/bin/sh

printf "Welcome to Sneed's arch auto install script, first we're going to partition the disk, please pick the disk you want partitioned:"

lsblk -l | grep disk

read disk1

diskchosen=$(lsblk -l | grep -w $disk1 | awk '{print $4}' | sed 's/G//') #gives us just the size of the chosen disk
MEM=$(free --giga | sed -n 2p | awk '{print $2}') #gives us the size of available memory
disk_space=$(echo "$diskchosen - 1.5*$MEM" | bc -lq) #calculates available disk space after taking into account the swap space

(
echo o # Create a new empty DOS partition table
echo n # Add a new partition
echo p # Primary partition
echo 1 # Creates the first partition: boot
echo   # First sector (Accept default: 1)
echo +100M  # Last sector
echo n
echo p
echo 2 #creates the root partition
echo 
echo +$(echo "$disk_space")G
echo n
echo p
echo 3 #creates swap space
echo
echo
echo w # Write changes
) | fdisk /dev/$disk1

mkfs.ext4 /dev/$(echo "$disk1")1
mkfs.ext4 /dev/$(echo "$disk1")2
mkswap /dev/$(echo "$disk1")3

mkdir /mnt/boot
mount /dev/$(echo "$disk1")1 /mnt/boot
mount /dev/$(echo "$disk1")2 /mnt/
swapon /dev/$(echo "disk1")3

pacstrap /mnt base base-devel

genfstab -U /mnt >> /mnt/etc/fstab

#curl https://gitlab.com/sneed/sneeds-arch-auto-install-scripts/raw/master/installpart2.sh -o installpart2.sh
#chmod +x installpart2.sh 
printf "This concludes part 1 of the install script, you'll have to download and run part 2. \n Please press any key to continue... "
arch-chroot /mnt/

#& echo "$disk1" | ./installpart2.sh 
