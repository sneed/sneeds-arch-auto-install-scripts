#!/bin/sh

disk1=$(lsblk -l | grep '/'  | cut -d' ' -f1 | sed -n 1p | cut -c 1-3)

printf "The base system is now installed, the following steps is now to configure your arch system. What would you like for this machine to be called? E.g:pc"
read pcname1

printf "Next is the time zone, what region are you in? Please type it out exactly how you see it.\n
Africa\n
Antarctica\n
Artic\n
America\n
Asia\n
Austrailia\n
Europe\n
Pacific
"
read region1

printf "Now select the city:"

ls /usr/share/zoneinfo/$region1

read city1

ln -sf /usr/share/zoneinfo/$region1/$city1 /etc/localtime

hwclock --systohc

printf "en_US.UTF-8 UTF-8" >> /etc/locale.gen

touch /etc/locale.conf
printf "LANG=en_US.UTF-8" >> /etc/locale.conf

touch /etc/hostname
printf "$pcname1" > /etc/hostname

touch /etc/hosts
printf "
127.0.0.1	localhost\n
::1		localhost\n
127.0.0.1	$pcname1.localdomain $pcname1" > /etc/hosts

curl -O https://gitlab.com/sneed/sneeds-dotfiles/raw/master/package_list
pacman -S $(cat package_list) 
grub-install --target=i386-pc /dev/$disk1
grub-mkconfig -o /boot/grub/grub.cfg
systemctl enable NetworkManager.service

printf "Now input your username:"
read user1
useradd -m -G wheel $user1
printf "Input your password:"
passwd $user1
printf "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

cd /home/$user1
git clone https://gitlab.com/sneed/sneeds-dotfiles.git 
cd sneeds-dotfiles
cp -R . /home/$user1
cd ..
git clone https://gitlab.com/sneed/sneeds-st-build.git
cd sneeds-st-build
make
make install
cd sneed-dmenu
make
make install
cd /home/$user1
git clone https://github.com/LukeSmithxyz/dwm.git
git clone https://github.com/LukeSmithxyz/dwmblocks.git
cd /
chown $user -R /home/$user

printf "Everything should be set up,restart your computer and unplug the flashdrive you've used to install arch."
exit




